import { SearchOutlined } from '@ant-design/icons';
import { Button, Input, Space, Table, Tag } from 'antd';
import React, { Fragment, useRef, useState } from 'react';
import data1 from "../../assets/json/data.json"
import Highlighter from 'react-highlight-words';
import { ChatService, GHI_NHO, LANG_NGHE, PhoneService, TAM_LY, TU_VAN, VideoService } from '../../config';
import {
    CheckCircleOutlined,
    ClockCircleOutlined,
    CloseCircleOutlined,
    ExclamationCircleOutlined,
    MinusCircleOutlined,
    SyncOutlined,
    VideoCameraOutlined
} from '@ant-design/icons';
console.log('data1: ', data1);
// const data = [
//     {
//         key: '1',
//         name: 'John Brown',
//         age: 32,
//         address: 'New York No. 1 Lake Park',
//     },
//     {
//         key: '2',
//         name: 'Joe Black',
//         age: 42,
//         address: 'London No. 1 Lake Park',
//     },
//     {
//         key: '3',
//         name: 'Jim Green',
//         age: 32,
//         address: 'Sidney No. 1 Lake Park',
//     },
//     {
//         key: '4',
//         name: 'Jim Red',
//         age: 32,
//         address: 'London No. 2 Lake Park',
//     },
// ];
const data = data1.data.advisorProfileCollection.items;  //! my data


export default function TableComponent() {
    const [searchText, setSearchText] = useState('');
    const [searchedColumn, setSearchedColumn] = useState('');
    const searchInput = useRef(null);
    const [isHorizontalTable, setIsHorizontalTable] = useState(false);
    const horizontalClass = isHorizontalTable ? "horizontal-table" : "";

    const handleSearch = (selectedKeys, confirm, dataIndex) => {
        confirm();
        setSearchText(selectedKeys[0]);
        setSearchedColumn(dataIndex);
    };

    const handleReset = (clearFilters) => {
        clearFilters();
        setSearchText('');
    };
    const getColumnSearchProps = (dataIndex) => ({
        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
            <div
                style={{
                    padding: 8,
                }}
            >
                <Input
                    ref={searchInput}
                    placeholder={`Search ${dataIndex}`}
                    value={selectedKeys[0]}
                    onChange={(e) => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
                    style={{
                        marginBottom: 8,
                        display: 'block',
                    }}
                />
                <Space>
                    <Button
                        type="primary"
                        onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
                        icon={<SearchOutlined />}
                        size="small"
                        style={{
                            width: 90,
                        }}
                    >
                        Search
                    </Button>
                    <Button
                        onClick={() => clearFilters && handleReset(clearFilters)}
                        size="small"
                        style={{
                            width: 90,
                        }}
                    >
                        Reset
                    </Button>
                    <Button
                        type="link"
                        size="small"
                        onClick={() => {
                            confirm({
                                closeDropdown: false,
                            });
                            setSearchText(selectedKeys[0]);
                            setSearchedColumn(dataIndex);
                        }}
                    >
                        Filter
                    </Button>
                </Space>
            </div>
        ),
        filterIcon: (filtered) => (
            <SearchOutlined
                style={{
                    color: filtered ? '#1890ff' : undefined,
                }}
            />
        ),
        onFilter: (value, record) => {
            return (
                record[dataIndex].toString().toLowerCase().includes(value.toLowerCase())
            )
        }
        ,
        onFilterDropdownOpenChange: (visible) => {
            if (visible) {
                setTimeout(() => searchInput.current?.select(), 100);
            }
        },
        render: (text) => {
            return (
                searchedColumn === dataIndex ? (
                    <Highlighter
                        highlightStyle={{
                            backgroundColor: '#ffc069',
                            padding: 0,
                        }}
                        searchWords={[searchText]}
                        autoEscape
                        textToHighlight={text ? text.toString() : ''}
                    />
                ) : (
                    text
                )
            )
        }

    });

    //! my functions
    const renderService = (serviceArray) => {
        return serviceArray.map((item, index) => {
            return (
                <div >
                    {
                        renderNameService(item.name)
                    }
                </div>
            )
        })
    }
    const renderNameService = (itemName) => {
        switch (itemName) {
            case VideoService:
                return (
                    <Tag color="magenta">Video</Tag>
                )
            case PhoneService:
                return (
                    <Tag color="green">Phone</Tag>
                )
            case ChatService:
                return (
                    <Tag color="cyan">Chat</Tag>
                );

            default:
                return;
        }
    }
    const renderCategory = (categoryArray) => {
        return categoryArray.map((Category, index) => {
            return (
                <div>
                    <p className="relative cursor-pointer w-max two my-0 mx-0 p-0">
                        <span>{Category.displayName}</span>
                        <span className="absolute -bottom-1 left-1/2 w-0 transition-all h-1 bg-yellow-400"></span>
                        <span className="absolute -bottom-1 right-1/2 w-0 transition-all h-1 bg-yellow-400"></span>
                    </p>
                </div>
            )
        })
    }
    const renderNameSkill = (skillName) => {
        switch (skillName) {
            case TAM_LY:
                return <Tag color="orange">{skillName}</Tag>

            case TU_VAN:
                return <Tag color="geekblue">{skillName}</Tag>
                break;
            case LANG_NGHE:
                return <Tag color="red">{skillName}</Tag>
                break;
            case GHI_NHO:
                return <Tag color="lime">{skillName}</Tag>
                break;

            default:
                break;
        }
    }
    const renderSkill = (SkillArray) => {
        return SkillArray.map((skill, index) => {
            return (
                <div className=''>
                    {renderNameSkill(skill.displayName)}
                </div>
            )
        })
    }
    // const columns = [
    //     {
    //         title: 'Name',
    //         dataIndex: 'name',
    //         key: 'name',
    //         width: '30%',
    //         ...getColumnSearchProps('name'),
    //     },
    //     {
    //         title: 'Age',
    //         dataIndex: 'age',
    //         key: 'age',
    //         width: '20%',
    //         ...getColumnSearchProps('age'),
    //     },
    //     {
    //         title: 'Address',
    //         dataIndex: 'address',
    //         key: 'address',
    //         ...getColumnSearchProps('address'),
    //         sorter: (a, b) => a.address.length - b.address.length,
    //         sortDirections: ['descend', 'ascend'],
    //     },
    // ];
    const columns = [
        {
            title: 'Name',
            dataIndex: 'displayName',
            key: 'displayName',
            // width: '30%',
            ...getColumnSearchProps('displayName'),

        },
        {
            title: 'Email',
            dataIndex: 'email',
            key: 'email',
            render: (text, record, index) => {
                return (
                    <div>
                        {
                            text ?
                                < p className='m-0 p-0' > {text}</p >
                                :
                                <Tag color="red">None</Tag>
                        }
                    </div>
                )
            }
            // width: '20%',
            // ...getColumnSearchProps('age'),
        },
        {
            title: 'Number phone',
            dataIndex: 'phone',
            key: 'phone',
            render: (text, record, index) => {
                return (
                    <div>
                        {
                            text ?
                                < p className='m-0 p-0' > {text}</p >
                                :
                                <Tag color="red">None</Tag>
                        }
                    </div>
                )
            }
        },
        {
            title: <p className='h-14 p-0 m-0'>Avatar</p>,
            dataIndex: 'avatarUrl',
            key: 'avatarUrl',
            render: (text, record, index) => {
                return (
                    <>
                        {text?.url ?
                            <div className='flex justify-center items-center'>
                                < img className='h-14 ' src={text?.url} alt={text?.title} />
                            </div>

                            :
                            <div className='h-14'>
                                <Tag color="red" >None</Tag>
                            </div>
                        }

                    </>
                )
            }
            // ...getColumnSearchProps('address')
        },
        {
            title: <p className='h-16 m-0 p-0'>Service</p>,
            dataIndex: 'servicesCollection',
            key: 'servicesCollection',
            // ...getColumnSearchProps('address')
            render: (text, record, index) => {
                return (
                    <div className='h-16'>
                        {text.items.length != 0 ? renderService(text.items) :
                            <div className='h-14'>
                                <Tag color="red" >None</Tag>
                            </div>
                        }
                    </div>
                )
            }
        },
        {
            title: <p className='h-16 m-0 p-0'>Category</p>,
            dataIndex: 'categoriesCollection',
            key: 'categoriesCollection',
            // ...getColumnSearchProps('address')
            render: (text, record, index) => {
                console.log('record: ', record.categoriesCollection.items);
                return (
                    <div className='h-16'>
                        {

                            text.items.length != 0 ? renderCategory(text.items) :
                                <div className='h-14'>
                                    <Tag color="red" >None</Tag>
                                </div>
                            // renderCategory(text.items)
                        }
                    </div>
                )
            },
            filters: [
                {
                    text: 'Tư vấn tâm lý',
                    value: 'Tư vấn tâm lý',
                },
                {
                    text: 'Xem phong thủy',
                    value: 'Xem phong thủy',
                },
            ]
            ,
            // onFilter: (value, record, index) => {
            //     console.log('value: ', value);
            //     console.log(' record.categoriesCollection.items', record.categoriesCollection.items);
            //     const myArray = record.categoriesCollection.items.map((item, index) => {

            //         // item.displayName
            //         // console.log('item.displayName: >>>>>>>>>>>>>>>>', item.displayName);
            //         return item.displayName.indexOf(value);
            //     })
            //     console.log('myArray: ', myArray);
            //     return (
            //         // record.categoriesCollection.indexOf(value) === 0
            //         // console.log(record.categoriesCollection.indexOf(value));
            //         // false
            //         myArray
            //     )
            // },
            onFilter: (value, record) => {
                console.log('value: ', value);
                console.log('record: ', record.categoriesCollection.items);
                let isFound = false;
                record.categoriesCollection.items.forEach((item, index) => {
                    if (item.displayName == value) {
                        console.log('item: >>>>>>>', item.displayName);
                        isFound = true;
                    }
                })
                return isFound;
            },
        },
        {
            title: <p className='h-20 m-0 p-0'>Skill</p>,
            dataIndex: 'skillsCollection',
            key: 'skillsCollection',
            // ...getColumnSearchProps('address')
            render: (text, record, index) => {
                return (
                    <div className='h-20'>
                        {
                            text.items.length != 0 ? renderSkill(text.items) :
                                <div>
                                    <Tag color="red" >None</Tag>
                                </div>
                        }
                    </div>
                )
            }
        },
    ];
    return (
        <div className='h-screen relative'>
            <div className='px-32 mt-10 absolute'
            >
                <button className='bg-red-600'
                    onClick={() => {
                        setIsHorizontalTable(!isHorizontalTable);
                    }}>
                    Toggle
                </button>
            </div>
            <div className='flex justify-center items-center h-full'>

                <div className="container overflow-y-auto">
                    <Table
                        scroll={{
                            x: "max-content"
                        }}
                        columns={columns}
                        className={`${horizontalClass}`}
                        dataSource={data}
                        pagination={{ pageSize: 5 }}
                    />
                </div>
            </div>
        </div>

    )
}
