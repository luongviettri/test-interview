import logo from './logo.svg';
import './App.css';
import TableComponent from './Components/Table/TableComponent';


function App() {
  return (
    <div className='mx-auto '>
      <TableComponent />
    </div>
  );
}

export default App;
